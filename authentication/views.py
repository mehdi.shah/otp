from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist

import pyotp
import base64

from datetime import datetime

from rest_framework.response import Response
from rest_framework.views import APIView

from authentication.models import phoneModel

#from twilio.rest import Client
from kavenegar import *



# This class returns the string needed to generate the key
class generateKey:
    @staticmethod
    def returnValue(phone):
        return str(phone) + str(datetime.date(datetime.now())) + "Some Random Secret Key"


class getPhoneNumberRegistered(APIView):
    # Get to Create a call for OTP
    @staticmethod
    def get(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)  # if Mobile already exists the take this else create New One
        except ObjectDoesNotExist:
            phoneModel.objects.create(
                Mobile=phone,
            )
            Mobile = phoneModel.objects.get(Mobile=phone)  # user Newly created Model
        Mobile.counter += 1  # Update Counter At every Call
        Mobile.save()  # Save the data
        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Key is generated
        OTP = pyotp.HOTP(key)  # HOTP Model for OTP is created
        print(OTP.at(Mobile.counter))
        # Using Multi-Threading send the OTP Using Messaging Services like Twilio or Fast2sms
        return Response({"otp": OTP.at(Mobile.counter)}, status=200)  # Just for demonstration

    # This Method verifies the OTP
    @staticmethod
    def post(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)
        except ObjectDoesNotExist:
            return Response("User does not exist", status=404)  # False Call

        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Generating Key
        OTP = pyotp.HOTP(key)  # HOTP Model
        if OTP.verify(request.data["otp"], Mobile.counter):  # Verifying the OTP
            Mobile.isVerified = True
            Mobile.save()
            
            return Response("You are authorized, thanks!", status=200)
        return Response("OTP is wrong, try again please.", status=400)





# Time after which OTP will expire
EXPIRY_TIME = 600 # seconds

class getPhoneNumberRegistered_TimeBased(APIView):
    # Get to Create a call for OTP
    @staticmethod
    def get(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)  # if Mobile already exists the take this else create New One
        except ObjectDoesNotExist:
            phoneModel.objects.create(
                Mobile=phone,
            )
            Mobile = phoneModel.objects.get(Mobile=phone)  # user Newly created Model
        Mobile.save()  # Save the data
        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Key is generated
        OTP = pyotp.TOTP(key,interval = EXPIRY_TIME)  # TOTP Model for OTP is created
        print(OTP.now())
        """print("**************************************************")
        account_sid = 'AC83bc1a654e85a044e85450f0ffe0a902'
        auth_token = 'e9e65a28e8a37a5df7fe926b28ff06d2'
        client = Client(account_sid, auth_token)
        message = client.messages.create(
            body='Hi there,',
            from_='+16179345585',
            to='+989197672179',
        )
        print("####################################################")
        print(message.sid)"""
        """api = KavenegarAPI('414946694A6D49413554424D4356584D3359497461426235444F544C5343676C596138737A3042776437343D')
        params = { 'sender' : '1000596446', 'receptor': phone, 'message' : {"your activate code is: \n", OTP.now()}}    
        response = api.sms_send( params)"""
        # Using Multi-Threading send the OTP Using Messaging Services like Twilio or Fast2sms
        return Response({"otp": OTP.now()}, status=200)  # Just for demonstration

    # This Method verifies the OTP
    @staticmethod
    def post(request, phone):
        try:
            Mobile = phoneModel.objects.get(Mobile=phone)
        except ObjectDoesNotExist:
            return Response("User does not exist", status=404)  # False Call

        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(phone).encode())  # Generating Key
        OTP = pyotp.TOTP(key,interval = EXPIRY_TIME)  # TOTP Model 
        if OTP.verify(request.data["otp"]):  # Verifying the OTP
            Mobile.isVerified = True
            Mobile.save()
            return Response("You are authorized, thanks!", status=200)
        return Response("OTP is wrong/expired, try again please.", status=400)
